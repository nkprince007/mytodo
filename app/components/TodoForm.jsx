import React from 'react';

class TodoForm extends React.Component {
    render() {
        let input;
        return (
            <form onSubmit={(event) => {event.preventDefault()}}>
                <input ref={node => {
                    input = node;
                }} />
                <button onClick={() => {
                    this.props.addTodo(input.value);
                    input.value = '';
                }}> Add </button>
            </form>
        );
    }
};

export default TodoForm;
