import React from 'react';
import TodoItem from './TodoItem';

const TodoList = ({ todos, remove, markFinished }) => {
    const todoNode = todos.map((todo) => {
        return (<TodoItem todo={todo} key={todo.id} remove={remove} markFinished={markFinished}/>)
    });
    return (<ul style={{listStyle: 'none'}}>{todoNode}</ul>);
}

export default TodoList;
