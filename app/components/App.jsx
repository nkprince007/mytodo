import React, {Component} from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';

class App extends Component {
    constructor() {
        super();

        this.state = { todos: [{id: 0, text: 'hello', checked: false}] };
        this.remove = this.remove.bind(this);
        // this.addTodo = this.addTodo.bind(this);
        this.markFinished = this.markFinished.bind(this);
    }

    markFinished(id) {
        const todo = this.state.todos[id];
        todo.checked = !todo.checked;
        this.setState({todos: this.state.todos})
    }

    remove(id) {
        const { todos } = this.state;
        todos.splice(id, 1);

        // reset id's
        let index = 0;
        todos.forEach(t => {
            t.id = index;
            index++;
        });

        this.setState({ todos });
    };

    addTodo(text) {
        debugger;
        let { todos } = this.state;
        todos = todos.concat([{ text, id: todos.length, checked: false }])
        this.setState({ todos });
    };

    render() {
        const {todos} = this.state;
        const total = todos.length;
        const finished = todos.filter(t => t.checked).length;
        return (
            <div>
                <TodoForm addTodo={this.addTodo} />
                <TodoList todos={this.state.todos} remove={this.remove} markFinished={this.markFinished} />
                <div>
                    <p>Total: {total}</p>
                    <p>Completed: {finished}</p>
                </div>
            </div>
        )
    }
};

export default App;
