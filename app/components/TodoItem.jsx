import React from 'react';

class TodoItem extends React.Component {
    render() {
        const { todo, remove, markFinished } = this.props;
        return (
            <li>
                <button onClick={() => {
                    remove(todo.id);
                }}>Delete</button>
                <input type="checkbox"
                    value={todo.checked ? "checked" : "unchecked"}
                    onChange={() => markFinished(todo.id)} />
                <span style={{textDecoration: todo.checked ? "line-through": "none" }}>{todo.text}</span>
            </li>
        );
    }
}

export default TodoItem;
